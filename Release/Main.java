import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import javax.sound.sampled.*;

import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;

public class Main{
	public static void main(String[] args){
		new Home();
		new Title();		
	}
}

class Title{
	private JFrame title;
	private JTextArea info;

	private Color windTitle, colInf;

	public Title(){
		title = new JFrame("[ Title ]");
		info = new JTextArea("");

		colInf = new Color(190, 190, 190);
		windTitle = new Color(160, 160, 160);

		info.setEditable(false);
		info.setBounds(20, 20, 640, 230);
		info.setBackground(colInf);
		info.setFont(new Font("Arial", Font.PLAIN, 21));
		info.setText("\t               [ Clicer story ]\n\nHello young fighter! You arrived at the Arena of the Pacific,\nthe strongest fighters of the world gather here.\nDo you want to get stronger? Yes?\nThen try to beat the champion with 1m points.\nOutrun him then you will become the strongest man on earth.");

		title.add(info);
		title.getContentPane().setBackground(windTitle);
		title.setSize(690, 300);
		title.setLocation(590, 300);
		title.setLayout(null);
		title.setVisible(true);
		title.setResizable(false);
		title.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
}

class Home{
	private JFrame home;

	private	JLabel storyClicker;
	private	JLabel txtOne, txtTwo;
	private JLabel txtMusic;
	private JLabel txtRes;
	private JLabel glasses;
	private JLabel force;

	private JButton clickBtn;
	private JButton playM;
	private JButton resGlasses;

	private Color cOf, cBo, cBm;

	private int frc = 1;
	private int glssLine = 0;

	public Home(){

		//read file with glasses and passing the value to a string
		try{
			BufferedReader br = new BufferedReader(new FileReader("./features/glasses/glasses.txt"));
			String line;
			while((line = br.readLine()) != null){
				System.out.println(line);
				glssLine = Integer.parseInt(line);
			}
			br.close();
		}
		catch(IOException ex){
			ex.printStackTrace();
		}

		home = new JFrame("[ Clicker story ]");
		storyClicker = new JLabel("[ Battlefield ]");
		txtOne = new JLabel("Glasses: ");
		glasses = new JLabel("0");
		txtTwo = new JLabel("Force: ");
		force = new JLabel("1");
		clickBtn = new JButton("[ Click ]");
		txtMusic = new JLabel("Music:");
		playM = new JButton("y");
		txtRes = new JLabel("Restert:");
		resGlasses = new JButton("y");


		storyClicker.setBounds(185, 10, 150, 20);
		txtOne.setBounds(10, 30, 100, 20);
		glasses.setBounds(10, 50, 200, 20);
		txtTwo.setBounds(390, 30, 100, 20);
		force.setBounds(390, 50, 100, 20);
		clickBtn.setBounds(50, 140,  400, 100);
		txtMusic.setBounds(390, 300, 100, 50);
		playM.setBounds(450, 300, 42, 45);
		txtRes.setBounds(10, 300, 100, 50);
		resGlasses.setBounds(80, 300, 42, 45);

		storyClicker.setFont(new Font("Arial", Font.PLAIN, 20));
		txtOne.setFont(new Font("Arial", Font.PLAIN, 18));
		glasses.setFont(new Font("Arial", Font.PLAIN, 18));
		txtTwo.setFont(new Font("Arial", Font.PLAIN, 18));
		force.setFont(new Font("Arial", Font.PLAIN, 18));
		clickBtn.setFont(new Font("Arial", Font.PLAIN, 30));
		txtMusic.setFont(new Font("Arial", Font.PLAIN, 18));
		playM.setFont(new Font("Arial", Font.PLAIN, 13));
		txtRes.setFont(new Font("Arial", Font.PLAIN, 18));
		resGlasses.setFont(new Font("Arial", Font.PLAIN, 13));


		cOf = new Color(160, 160, 160);
		cBo = new Color(255, 51, 51);
		cBm = new Color(135, 135, 135);
		clickBtn.setBackground(cBo);
		playM.setBackground(cBm);
		resGlasses.setBackground(cBm);
		home.getContentPane().setBackground(cOf);
		
		home.add(storyClicker);
		home.add(txtOne);
		home.add(glasses);
		home.add(txtTwo);
		home.add(force);
		home.add(clickBtn);
		home.add(txtMusic);
		home.add(playM);
		home.add(txtRes);
		home.add(resGlasses);

		home.setSize(500, 380);
		home.setLocation(700, 255);
		home.setLayout(null);
		home.setVisible(true);
		home.setResizable(false);
		home.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		clickBtn.addActionListener(new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e){
				String s = e.getActionCommand();
				if(s.equals("[ Click ]")){
					glssLine += frc;
					glasses.setText(Integer.toString(glssLine));

					if(glssLine == 1000000){
						new Win();
					}
				
					//Write value in file with glasses
					String a = Integer.toString(glssLine);
					try{
						FileWriter writeObj = new FileWriter("./features/glasses/glasses.txt", false);
						writeObj.write(a);
						writeObj.close();
					}
					catch(IOException ex){
						ex.printStackTrace();
					}
			

				}
			}
		});

		playM.addActionListener(new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e){
				String s = e.getActionCommand();
				if(s.equals("y")){
					try{
						new Player();
						playM.setEnabled(false);
					}
					catch(Exception ex){
						System.out.println("Error with playing sound");
						ex.printStackTrace();
					}	


				}
			}
		});

		resGlasses.addActionListener(new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e){
				String s = e.getActionCommand();
				if(s.equals("y")){
					glssLine = 0;
					glasses.setText(Integer.toString(glssLine));

					//зброс данных с файла
					//Values reset with glasses file
					String a = Integer.toString(0);
					try{
						FileWriter writeObj = new FileWriter("./features/glasses/glasses.txt", false);
						writeObj.write(a);
						writeObj.close();
					}
					catch(IOException ex){
						ex.printStackTrace();
					}

				}
			}
		});
	}
}

class Player{
	//play music
	public Player() throws UnsupportedAudioFileException, IOException, LineUnavailableException{
		Scanner scanner = new Scanner(System.in);
		File file = new File("./features/sounds/Sport Rock Country by Infraction -No Copyright Music- Dark Western.wav");
		AudioInputStream audioStream = AudioSystem.getAudioInputStream(file);
		Clip clip = AudioSystem.getClip();
		clip.open(audioStream);
		clip.loop(Clip.LOOP_CONTINUOUSLY);
		
		clip.start();
	}
}

class Win extends Canvas{
	private JFrame win;
	private JPanel panel;
	private JTextArea info;

	private Color windWin, colInf;
	
	private File fPic;
	BufferedImage picture;
	private JLabel picLabel;

	public Win() {
		win = new JFrame("[ Win ]");
		panel = new JPanel();
		info = new JTextArea("");

		colInf = new Color(190, 190, 190);
		windWin = new Color(160, 160, 160);

		info.setEditable(false);
		info.setBounds(20, 20, 640, 300);
		info.setBackground(colInf);
		info.setFont(new Font("Arial", Font.PLAIN, 21));
		info.setText("\t                    [ Vinctory ]\n\nSo much times has passed since our last meeting meeting\nwayfarer. Countless victories and defeats and here you're,\nthe strongest warrior. Congratulations on this victory!\nPeace be with you! Success to you in every endeavor.\n\n\n\nAutor - helicopter");

		panel.setBounds(20, 330, 655, 135);
		panel.setBackground(windWin);

		//We put the picture in the JLabel (only work in Jpanel)
		try{
			fPic = new File("./features/art/win.jpg");
			picture = ImageIO.read(fPic);
			picLabel = new JLabel(new ImageIcon(picture));
		}
		catch(IOException e){
			e.printStackTrace();
		}

		panel.add(picLabel);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER));

		win.add(panel);
		win.add(info);
		win.getContentPane().setBackground(windWin);
		win.setSize(700, 500);
		win.setLocation(590, 300);
		win.setLayout(null);
		win.setVisible(true);
		win.setResizable(false);
		win.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
}
