# Klicer_story

Hello young fighter! You arrived at the Arena of the Pacific, the strongest fighters of the world gather here.
Do you want to get stronger? Yes?\nThen try to beat the champion with 1m points. Outrun him then you will become the strongest man on earth.

---

### This game has several nice advantages:

- [ ] Saving results and resetting them if desired
- [ ] There is music
- [ ] The game can pass

---

### File compilation

Compile the main file and run it class

```
javac Main.java
java Main
```

Have a nice game ( ノ ^o^)ノ

---

![](./Release/winds.png)



